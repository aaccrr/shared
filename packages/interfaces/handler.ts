export interface IHandler<T, U> {
  run(Request: T): Promise<U>;
}
