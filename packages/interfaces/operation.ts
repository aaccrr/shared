import { RequestPayload } from "./request";

export interface IOperation<T, U, H> {
  mapper: IOperationMapper<T>;
  validator: IOperationValidator<U>;
  handler: H;
  access: IOperationAccess;
}

export interface IOperationMapper<T> {
  (ctx: RequestPayload, next?): Promise<T>;
}
export interface IOperationValidator<T> {
  (request: T): Promise<void>;
}

export interface IOperationAccess {
  auth: boolean;
  roles: string[];
}
