export type RequestPayload = any;

export interface IRequestMeta {
  gateway: "client" | "admin" | "office";
  ip: string;
  user?: any;
}
