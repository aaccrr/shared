export interface IDbDeleteResponse {
  isDeleted: boolean;
}

export interface IDbUpdateResponse {
  isModified: boolean;
  isMatched: boolean;
}
export interface IDBConfig {
  user: string;
  password: string;
  host: string;
  port: number;
  database: string;
}
