import { mapping } from "./mapping";

export class RpcClient {
  private mapping: any = {};

  init(): void {
    Object.keys(mapping).forEach(key => {
      const map = mapping[key];
      map.init();
    });
    this.mapping = mapping;
  }

  service(serverName: string) {
    return this.mapping[serverName];
  }
}
