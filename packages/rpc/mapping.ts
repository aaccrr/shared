import { Client } from "jayson";

export const mapping = {
  account: {
    client: null,
    config: {
      host: "localhost",
      port: 50051
    },
    init() {
      this.client = Client.http(this.config);
    },
    methods: {
      view(request) {
        return new Promise((resolve, reject) => {
          this.client.request("view", request, (err, res) => {
            if (err) return reject(err);
            return resolve(res);
          });
        });
      },
      create() {}
    }
  }
};
