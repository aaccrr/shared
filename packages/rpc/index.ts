import { RpcClient } from "./client";
import { RpcServer } from "./server";

const Client = new RpcClient();
const Server = new RpcServer();

export { Client as RpcClient, Server as RpcServer };
