import * as jayson from "jayson";

export class RpcServer {
  init(config) {
    const server = new jayson.Server(config.routes);

    server.http().listen(config.port);
  }
}
