import * as request from "request-promise-native";

export const get = (url: string) => {
  return async (u: string, query: object) => {
    url = url + u;
    try {
      const res = await request.get(url, { qs: query, json: true });
      return res.data;
    } catch (e) {
      return null;
    }
  };
};

export const post = (url: string) => {
  return async (u: string, body: object) => {
    url = url + u;
    const res = await request.post(url, { body, json: true });
    return res.data;
  };
};
