import { get, post } from "./methods";

export const routes = {
  account: {
    url: "/account",
    methods: {
      list: get,
      view: get
    }
  },
  registration: {
    url: "/auth/registration",
    methods: {
      registration: post
    }
  },
  organization: {
    url: "/organization",
    view: get,
    create: post
  },
  ticket: {
    url: "/ticket",
    methods: {
      view: get,
      list: get,
      create: post
    }
  },
  abonement: {
    url: "/abonement",
    methods: {
      view: get,
      list: get,
      create: post
    }
  },
  event: {
    url: "/event",
    methods: {
      view: get,
      list: get,
      create: post
    }
  }
};
