import { routes } from "./routes";

export function Fetcher(baseUrl: string) {
  return serviceName => {
    const route = routes[serviceName];
    if (!route) return;

    const fetcher: any = {};

    const url = baseUrl + route.url;
    Object.keys(route.methods).forEach(method => {
      fetcher[method] = route.methods[method](url);
    });

    return fetcher;
  };
}
